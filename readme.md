## Menedżer szkolnych projektów

Menedżer szkolnych projektów zaprojektowany dla systemu operacyjnego Manjaro.
Powinien działać z wszystkimi 64-bitowymi Linuxami, na których jest zainstalowane środowisko .NET Core.

# Główne funkcje:
- szybkie przełączanie się pomiędzy poszczególnymi przedmiotami (np. informatyka / witryny)
- organizacja projektów według daty
- możliwość tworzenia projektów w różnych językach programowania (gotowe szablony)
- możliwość łatwego udostępniania stron innternetowych w sieci lokalnej

# Jakie warunki musi spełniać system:
- 64-bitowy Linux
- zainstalowane środowisko .NET Core
- obecność menedżera plików Thunar
- serwer WWW z obsługą PHP przypięty do katalogu /srv/http
- uprawnienia 777 na katalogu /srv/http
- uprawnienia odczytu dla każdego użytkownika w katalogu ~
- zainstalowane Visual Studio Code (obecność komendy code)