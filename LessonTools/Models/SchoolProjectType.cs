﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LessonTools.Models
{
    public enum SchoolProjectType
    {
        CPP,
        HTML,
        PHP,
        CS
    }
}
