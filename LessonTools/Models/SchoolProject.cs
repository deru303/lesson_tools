﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LessonTools.Models
{
    public class SchoolProject
    {
        public string Name { get; private set; }

        public SchoolProjectType Type { get; private set; }

        public string DirectoryPath { get; private set; }

        public SchoolProject(string name, string directoryPath, SchoolProjectType type)
        {
            Name = name;
            Type = type;
            DirectoryPath = directoryPath;
        }
    }
}
