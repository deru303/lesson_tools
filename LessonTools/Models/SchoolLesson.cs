﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LessonTools.Models
{
    public class SchoolLesson
    {
        public DateTime CreationDate { get; private set; }

        public List<SchoolProject> Projects { get; private set; }

        public string Memo { get; private set; }

        public SchoolLesson(DateTime creationDate, List<SchoolProject> projects, string memo = "(brak notatki)")
        {
            CreationDate = creationDate;
            Projects = projects;
            Memo = memo;
        }
    }
}
