﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LessonTools.Models
{
    public class SchoolSubject
    {
        public string Name { get; private set; }

        public List<SchoolLesson> Lessons { get; private set; }

        public SchoolSubject(string name, List<SchoolLesson> lessons)
        {
            Name = name;
            Lessons = lessons;
        }
    }
}
