﻿using Avalonia;
using Avalonia.Controls;
using System.Linq;
using Avalonia.Markup.Xaml;
using LessonTools.Views;
using System.Reflection;

namespace LessonTools
{
    public class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            #if DEBUG
            this.AttachDevTools();
            #endif
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
            this.Content = new SubjectPickerWindow(this).Content;

            string pathToIcon = $"LessonTools.Icon.ico";
            using(var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(pathToIcon))
                this.Icon = new WindowIcon(stream);
        }
    }
}
