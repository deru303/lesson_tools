﻿using Mono.Unix.Native;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System;

namespace LessonTools.OS
{
    public static class AdminPrivileges
    {
        public static bool HasAdmin;

        static AdminPrivileges()
        {
            if (UserOperatingSystem.UserOS == OS.Windows)
                HasAdmin = new WindowsPrincipal(WindowsIdentity.GetCurrent()).IsInRole(WindowsBuiltInRole.Administrator);
            else if (UserOperatingSystem.UserOS == OS.Linux)
                HasAdmin = Syscall.getuid() == 0;
            else
                throw new InvalidProgramException("This class does not support OSX.");
        }
    }
}
