﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace LessonTools.OS
{
    public class ShellCommand
    {
        private string Content;
        private bool WaitForExit;

        public ShellCommand(string commandContent, bool waitForExit = true)
        {
            Content = commandContent;
            WaitForExit = waitForExit;
        }

        public string Call()
        {
            string fileName = "";
            string arguments = "";

            if (UserOperatingSystem.UserOS == OS.Windows)
            {
                fileName = "cmd.exe";
                arguments = $"/C \"{Content}\"";
            }
            else if (UserOperatingSystem.UserOS == OS.Linux)
            {
                fileName = "/bin/bash";
                arguments = $"-c \"{Content}\"";
            }
            else
            {
                throw new InvalidProgramException("This class does not support OSX.");
            }
            Process process = new Process()
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = fileName,
                    Arguments = arguments,
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true
                }
            };
            process.Start();
            string result = process.StandardOutput.ReadToEnd();
            if(WaitForExit)
                process.WaitForExit();
            return result;
        }
    }
}
