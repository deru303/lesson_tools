﻿using System;

namespace LessonTools.OS
{
    [Flags]
    public enum OS
    {
        Linux = 1,
        Windows = 2,
        OSX = 4
    }
}
