﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace LessonTools.OS
{
    public static class UserOperatingSystem
    {
        public static OS UserOS
        {
            get
            {
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                    return OS.Windows;
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                    return OS.Linux;
                else
                    return OS.OSX;
            }
        }
    }
}
