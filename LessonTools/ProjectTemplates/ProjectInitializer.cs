﻿using LessonTools.OS;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace LessonTools.ProjectTemplates
{
    public static class ProjectInitializer
    {
        public static void InitializeCppApplication(string projectDir)
        {
            string mainCppPath = Path.Combine(projectDir, "main.cpp");
            File.WriteAllText(mainCppPath, ReadTemplateFile("CppTemplate.cpp"));
        }

        public static void InitializePhpSite(string projectDir)
        {
            string indexPhpPath = Path.Combine(projectDir, "index.php");
            File.WriteAllText(indexPhpPath, ReadTemplateFile("SiteTemplate.html"));
        }

        public static void InitializeHtmlSite(string projectDir)
        {
            string indexHtmlPath = Path.Combine(projectDir, "index.html");
            File.WriteAllText(indexHtmlPath, ReadTemplateFile("SiteTemplate.html"));
        }

        public static void InitializeCsApplication(string projectDir)
        {
            string command = (UserOperatingSystem.UserOS == OS.OS.Windows) ? 
                $"dotnet new console -o \"{projectDir}\"" : 
                $"dotnet new console -o '{projectDir}'";
            new ShellCommand(command).Call();
        }

        private static string ReadTemplateFile(string fileName)
        {
            string pathToFile = $"LessonTools.ProjectTemplates.TemplateFiles.{fileName}";
            using (var reader = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream(pathToFile)))
            {
                return reader.ReadToEnd();
            }

            string[] possibleCommands = new string[] {
                "run", "init", "new", "restore"
            };
            
            possibleCommands.Select(com => "dotnet " + com).OrderBy(GetLastOrder());
        }
    }
}
