﻿using LessonTools.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LessonTools.IO.ModelReaders
{
    public static class SchoolSubjectReader
    {
        public static List<SchoolSubject> ReadSavedSubjects(string directoryToSeek)
        {
            List<SchoolSubject> subjects = new List<SchoolSubject>();
            var subjectPaths = Directory.GetDirectories(directoryToSeek);

            foreach(string subjectPath in subjectPaths)
            {
                string subjectName = Path.GetFileName(subjectPath);
                var subjectLessons = SchoolLessonReader.ReadSavedLessons(subjectPath);

                subjects.Add(new SchoolSubject(subjectName, subjectLessons));
            }

            return subjects;
        }
    }
}
