﻿using LessonTools.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace LessonTools.IO.ModelReaders
{
    public static class SchoolProjectReader
    {
        public static List<SchoolProject> ReadSavedProjects(string directoryToSeek)
        {
            List<SchoolProject> projects = new List<SchoolProject>();
            var projectPaths = Directory.GetDirectories(directoryToSeek);

            foreach (string projectPath in projectPaths)
            {
                string projectFolderName = Path.GetFileName(projectPath);

                Regex regex = new Regex(@"\[([A-Za-z]+)\] ([\W\w\s\S\d\D]+)");
                Match match = regex.Match(projectFolderName);

                SchoolProjectType projectType = (SchoolProjectType)Enum.Parse(typeof(SchoolProjectType), match.Groups[1].Value.ToUpper());
                string projectName = match.Groups[2].Value;

                projects.Add(new SchoolProject(projectName, projectPath, projectType));
            }

            return projects;
        }
    }
}
