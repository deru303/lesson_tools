﻿using LessonTools.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LessonTools.IO.ModelReaders
{
    public static class SchoolLessonReader
    {
        public static List<SchoolLesson> ReadSavedLessons(string directoryToSeek)
        {
            List<SchoolLesson> lessons = new List<SchoolLesson>();
            var lessonPaths = Directory.GetDirectories(directoryToSeek);

            foreach(string lessonPath in lessonPaths)
            {
                DateTime creationDate = DateTime.Parse(Path.GetFileName(lessonPath));
                string memo = "(brak notatki)";
                if (File.Exists(lessonPath + Path.DirectorySeparatorChar + "notatka.txt"))
                    memo = File.ReadAllText(lessonPath + Path.DirectorySeparatorChar + "notatka.txt");
                List<SchoolProject> projects = SchoolProjectReader.ReadSavedProjects(lessonPath);
                lessons.Add(new SchoolLesson(creationDate, projects, memo));
            }

            return lessons;
        }
    }
}
