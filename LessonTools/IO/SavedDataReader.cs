﻿using LessonTools.IO.ModelReaders;
using LessonTools.Models;
using System;
using System.Collections.Generic;
using System.IO;

namespace LessonTools.IO
{
    class SavedDataReader
    {
        public static readonly string DataPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + Path.DirectorySeparatorChar + "Szkolne projekty";

        public SavedDataReader()
        {
            Directory.CreateDirectory(DataPath);
        }

        public List<SchoolSubject> ReadSavedData()
        {
            return SchoolSubjectReader.ReadSavedSubjects(DataPath);
        }
    }
}
