﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using LessonTools.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LessonTools.Views.Dialogs
{
    public class NewLessonDialog : Window
    {
        private MainWindow MainWindow;
        private LessonPickerWindow LastWindow;
        private List<SchoolLesson> ExistingLessons;
        private string PathToLessons;

        public NewLessonDialog(MainWindow mainWindow, LessonPickerWindow lastWindow, List<SchoolLesson> existingLessons, string pathToLessons)
        {
            MainWindow = mainWindow;
            LastWindow = lastWindow;
            ExistingLessons = existingLessons;
            PathToLessons = pathToLessons;
            this.InitializeComponent();
            #if DEBUG
            this.AttachDevTools();
            #endif
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);

            TextBox datePicker = this.FindControl<TextBox>("LessonCreationDate");
            datePicker.Text = DateTime.Now.ToString("dd.MM.yyyy");

            Button cancelButton = this.FindControl<Button>("CancelCreation");
            cancelButton.Click += (sender, args) => { LastWindow.Refresh(); MainWindow.Content = LastWindow.Content; };

            Button createLessonButton = this.FindControl<Button>("LessonCreate");
            createLessonButton.Click += (sender, args) =>
            {
                TextBlock errorMessage = this.FindControl<TextBlock>("ErrorMessage");

                try
                {
                    DateTime creationDate = DateTime.Parse(datePicker.Text);

                    if (ExistingLessons.Select(less => less.CreationDate).Contains(creationDate))
                    {
                        errorMessage.Text = "Lekcja o takiej dacie już istnieje!";
                    }
                    else
                    {
                        string pathToLesson = PathToLessons + Path.DirectorySeparatorChar + creationDate.ToString("dd.MM.yyyy");
                        Directory.CreateDirectory(pathToLesson);
                        ExistingLessons.Add(new SchoolLesson(creationDate, new List<SchoolProject>()));
                        LastWindow.Refresh();
                        MainWindow.Content = LastWindow.Content;
                    }
                }
                catch(FormatException)
                {
                    errorMessage.Text = "Nieprawidłowy format daty!";
                }
            };
        }
    }
}
