﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using System.IO;

namespace LessonTools.Views.Dialogs
{
    public class LessonMemoDialog : Window
    {
        private MainWindow MainWindow;
        private ProjectPickerWindow LastWindow;
        private string PathToMemo;

        public LessonMemoDialog(MainWindow mainWindow, ProjectPickerWindow lastWindow, string pathToMemo)
        {
            MainWindow = mainWindow;
            LastWindow = lastWindow;
            PathToMemo = pathToMemo;

            this.InitializeComponent();
            #if DEBUG
            this.AttachDevTools();
            #endif
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);

            string memoContent = "(brak notatki)";
            if (File.Exists(PathToMemo))
            {
                string fileContent = File.ReadAllText(PathToMemo);
                if(! string.IsNullOrWhiteSpace(fileContent))
                {
                    memoContent = fileContent;
                }
            }
            this.FindControl<TextBox>("LessonMemo").Text = memoContent;

            this.FindControl<Button>("MemoCreation").Click += (sender, args) =>
            {
                string memoCont = this.FindControl<TextBox>("LessonMemo").Text;
                if (string.IsNullOrWhiteSpace(memoCont))
                    memoCont = "(brak notatki)";
                File.WriteAllText(PathToMemo, memoCont);
                LastWindow.Refresh();
                MainWindow.Content = LastWindow.Content;
            };

            this.FindControl<Button>("CancelCreation").Click += (sender, args) =>
            {
                MainWindow.Content = LastWindow.Content;
            };
        }
    }
}
