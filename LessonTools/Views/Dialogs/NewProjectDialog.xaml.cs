﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using LessonTools.Models;
using LessonTools.ProjectTemplates;
using System.Collections.Generic;
using System.IO;

namespace LessonTools.Views.Dialogs
{
    public class NewProjectDialog : Window
    {
        private MainWindow MainWindow;
        private ProjectPickerWindow LastWindow;
        private List<SchoolProject> ExistingProjects;
        private string PathToProjects;

        public NewProjectDialog(MainWindow mainWindow, ProjectPickerWindow lastWindow, List<SchoolProject> existingProjects, string pathToProjects)
        {
            MainWindow = mainWindow;
            LastWindow = lastWindow;
            ExistingProjects = existingProjects;
            PathToProjects = pathToProjects;

            this.InitializeComponent();
            #if DEBUG
            this.AttachDevTools();
            #endif
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);

            this.FindControl<Button>("CancelCreation").Click += (sender, args) =>
            {
                MainWindow.Content = LastWindow.Content;
            };

            this.FindControl<Button>("ProjectCreate").Click += (sender, args) =>
            {
                string projectName = this.FindControl<TextBox>("NewProjectName").Text?.Trim();
                if (string.IsNullOrEmpty(projectName))
                {
                    this.FindControl<TextBlock>("ErrorMessage").Text = "Nazwa projektu nie może być pusta.";
                }
                else
                {
                    SchoolProjectType projectType = SchoolProjectType.CPP;
                    switch(this.FindControl<ComboBox>("NewProjectType").SelectedIndex)
                    {
                        case 0:
                            projectType = SchoolProjectType.CPP; break;
                        case 1:
                            projectType = SchoolProjectType.CS; break;
                        case 2:
                            projectType = SchoolProjectType.HTML; break;
                        case 3:
                            projectType = SchoolProjectType.PHP; break;
                    }
                    string pathToProject = Path.Combine(PathToProjects, $"[{projectType}] {projectName}");
                    if(Directory.Exists(pathToProject))
                    {
                        this.FindControl<TextBlock>("ErrorMessage").Text = "Projekt o takiej nazwie i takim typie już istnieje.";
                    }
                    else
                    {
                        Directory.CreateDirectory(pathToProject);
                        ExistingProjects.Add(new SchoolProject(projectName, pathToProject, projectType));
                        switch(projectType)
                        {
                            case SchoolProjectType.CPP:
                                ProjectInitializer.InitializeCppApplication(pathToProject); break;
                            case SchoolProjectType.CS:
                                ProjectInitializer.InitializeCsApplication(pathToProject); break;
                            case SchoolProjectType.HTML:
                                ProjectInitializer.InitializeHtmlSite(pathToProject); break;
                            case SchoolProjectType.PHP:
                                ProjectInitializer.InitializePhpSite(pathToProject); break;
                        }
                        LastWindow.Refresh();
                        MainWindow.Content = LastWindow.Content;
                    }
                }
            };
        }
    }
}
