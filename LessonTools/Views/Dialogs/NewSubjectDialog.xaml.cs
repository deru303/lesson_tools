﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using LessonTools.IO;
using System.IO;

namespace LessonTools.Views.Dialogs
{
    public class NewSubjectDialog : Window
    {
        private MainWindow MainWindow;
        private SubjectPickerWindow LastWindow;

        public NewSubjectDialog(MainWindow mainWindow, SubjectPickerWindow lastWindow)
        {
            MainWindow = mainWindow;
            LastWindow = lastWindow;

            this.InitializeComponent();
            #if DEBUG
            this.AttachDevTools();
            #endif      
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);

            Button cancelButton = this.FindControl<Button>("CancelCreation");
            cancelButton.Click += (sender, args) => { MainWindow.Content = LastWindow.Content; };

            Button createSubjectButton = this.FindControl<Button>("SubjectCreate");
            createSubjectButton.Click += (sender, args) => 
            {
                TextBox newSubjectName = this.FindControl<TextBox>("NewSubjectName");
                Directory.CreateDirectory(SavedDataReader.DataPath + Path.DirectorySeparatorChar + newSubjectName.Text);

                LastWindow.Refresh();
                MainWindow.Content = LastWindow.Content;
            };
        }
    }
}
