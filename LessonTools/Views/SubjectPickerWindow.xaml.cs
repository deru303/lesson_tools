﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Media;
using Avalonia.Markup.Xaml;
using LessonTools.IO;
using LessonTools.Models;
using System.Collections.Generic;
using LessonTools.Views.Dialogs;

namespace LessonTools.Views
{
    public class SubjectPickerWindow : Window
    {
        List<SchoolSubject> Subjects;
        MainWindow MainWindow;

        public SubjectPickerWindow(MainWindow mainWindow)
        {
            MainWindow = mainWindow;
            this.InitializeComponent();
            #if DEBUG
            this.AttachDevTools();
            #endif
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
            Refresh();
        }

        public void Refresh()
        {
            Subjects = new SavedDataReader().ReadSavedData();
            StackPanel buttons = this.FindControl<StackPanel>("SubjectButtons");
            buttons.Children.Clear();

            if(Subjects.Count < 1)
            {
                buttons.Children.Add(new TextBlock { Text = "Brak istniejących przedmiotów. Utwórz jakiś!", Margin = new Thickness(0, 0, 0, 4) });
            }

            foreach (SchoolSubject subject in Subjects)
            {
                Button subjectChoiceButton = new Button();
                subjectChoiceButton.Click += (sender, args) =>
                {
                    MainWindow.Content = new LessonPickerWindow(MainWindow, this, subject.Lessons, subject.Name).Content;
                };
                subjectChoiceButton.Content = subject.Name;
                buttons.Children.Add(subjectChoiceButton);
            }

            Button newSubjectButton = new Button();
            newSubjectButton.Content = "Utwórz nowy przedmiot";
            newSubjectButton.FontWeight = FontWeight.Bold;
            newSubjectButton.Click += (sender, args) => {
                MainWindow.Content = new NewSubjectDialog(MainWindow, this).Content;
            };
            buttons.Children.Add(newSubjectButton);

            Button shutdownButton = new Button();
            shutdownButton.Content = "Zakończ pracę programu";
            shutdownButton.FontWeight = FontWeight.Bold;
            shutdownButton.Click += (sender, args) => { Application.Current.Exit(); };
            buttons.Children.Add(shutdownButton);
        }
    }
}
