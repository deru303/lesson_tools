﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Media;
using Avalonia.Markup.Xaml;
using LessonTools.Models;
using System.Collections.Generic;
using LessonTools.Views.Dialogs;
using LessonTools.IO;
using System.IO;
using System.Linq;

namespace LessonTools.Views
{
    public class LessonPickerWindow : Window
    {
        private MainWindow MainWindow;
        private SubjectPickerWindow LastWindow;
        private List<SchoolLesson> LessonsToPick;
        private string SubjectName;

        public LessonPickerWindow(MainWindow mainWindow, SubjectPickerWindow lastWindow, List<SchoolLesson> lessonsToPick, string subjectName)
        {
            MainWindow = mainWindow;
            LastWindow = lastWindow;
            LessonsToPick = lessonsToPick;
            SubjectName = subjectName;
            this.InitializeComponent();
            #if DEBUG
            this.AttachDevTools();
            #endif
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
            Refresh();
        }

        public void Refresh()
        {
            StackPanel buttons = this.FindControl<StackPanel>("LessonButtons");
            buttons.Children.Clear();

            this.FindControl<TextBlock>("ChosenSubject").Text = SubjectName;

            if (LessonsToPick.Count < 1)
            {
                buttons.Children.Add(new TextBlock() { Text = "Brak istniejących lekcji. Utwórz jakąś!", Margin = new Thickness(0, 0, 0, 4) });
            }

            LessonsToPick = LessonsToPick.OrderBy(less => less.CreationDate).ToList();

            foreach (SchoolLesson lesson in LessonsToPick)
            {
                Button lessonButton = new Button() { Content = lesson.CreationDate.ToString("dd MMMM yyyy") };
                lessonButton.Click += (sender, args) => { LastWindow.Refresh(); MainWindow.Content = new ProjectPickerWindow(MainWindow, this, lesson.Projects, SubjectName, lesson.CreationDate.ToString("dd.MM.yyyy")).Content; };
                buttons.Children.Add(lessonButton);
            }

            Button newLessonButton = new Button() { Content = "Utwórz nową lekcję", FontWeight = FontWeight.Bold };
            newLessonButton.Click += (sender, args) => { MainWindow.Content = new NewLessonDialog(MainWindow, this, LessonsToPick, SavedDataReader.DataPath + Path.DirectorySeparatorChar + SubjectName).Content; };
            buttons.Children.Add(newLessonButton);


            Button goBackButton = new Button() { Content = "Powrót do poprzedniego menu", FontWeight = FontWeight.Bold };
            goBackButton.Click += (sender, args) => { LastWindow.Refresh(); MainWindow.Content = LastWindow.Content; };
            buttons.Children.Add(goBackButton);
        }
    }
}
