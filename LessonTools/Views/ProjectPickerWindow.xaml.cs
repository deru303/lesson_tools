﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.Media;
using LessonTools.IO;
using LessonTools.IO.ModelReaders;
using LessonTools.Models;
using LessonTools.Views.Dialogs;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LessonTools.Views
{
    public class ProjectPickerWindow : Window
    {
        private MainWindow MainWindow;
        private LessonPickerWindow LastWindow;
        private List<SchoolProject> ProjectsToPick;
        private string SubjectName, LessonName;

        private readonly FontFamily BtnFont = FontFamily.SystemFontFamilies.Where(font => font.Name == "Verdana").First();

        public ProjectPickerWindow(MainWindow mainWindow, LessonPickerWindow lastWindow, List<SchoolProject> projectsToPick, string subjectName, string lessonName)
        {
            MainWindow = mainWindow;
            LastWindow = lastWindow;
            ProjectsToPick = projectsToPick.OrderBy(proj => proj.DirectoryPath).ToList();
            SubjectName = subjectName; LessonName = lessonName;

            this.InitializeComponent();
            #if DEBUG
            this.AttachDevTools();
            #endif
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
            Refresh();
        }

        public void Refresh()
        {
            StackPanel buttons = this.FindControl<StackPanel>("ProjectButtons");
            buttons.Children.Clear();

            string memoPath = Path.Combine(SavedDataReader.DataPath, SubjectName, LessonName, "notatka.txt");

            if(File.Exists(memoPath))
                this.FindControl<TextBlock>("LessonMemoContent").Text = File.ReadAllText(memoPath);

            this.FindControl<TextBlock>("ChosenSubject").Text = SubjectName;
            this.FindControl<TextBlock>("ChosenLesson").Text = LessonName;

            ProjectsToPick = SchoolProjectReader.ReadSavedProjects(Path.Combine(SavedDataReader.DataPath, SubjectName, LessonName));

            if (ProjectsToPick.Count < 1)
            {
                buttons.Children.Add(new TextBlock() { Text = "Brak istniejących projektów. Utwórz jakiś!", Margin = new Thickness(0, 0, 0, 4) });
            }

            foreach (SchoolProject project in ProjectsToPick)
            {
                Button projectButton = new Button() { Content = new TextBlock() { Text = $"[{project.Type}] {project.Name}", Margin = new Thickness(0, 2) } };

                ContextMenu projectMenu = new ContextMenu();
                projectMenu.Items = new string[] { "Zmień nazwę projektu", "Usuń projekt" };

                projectButton.ContextMenu = projectMenu;
                projectButton.FontFamily = BtnFont;
                projectButton.Click += (sender, args) => { MainWindow.Content = new ProjectManipulatorWindow(MainWindow, this, SubjectName, LessonName, project); };
                buttons.Children.Add(projectButton);
            }


            Button newProjectButton = new Button() { FontWeight = FontWeight.Bold, Content = "Utwórz nowy projekt" };
            newProjectButton.Click += (sender, args) =>
            {
                MainWindow.Content = new NewProjectDialog(MainWindow, this, ProjectsToPick, Path.Combine(SavedDataReader.DataPath, SubjectName, LessonName)).Content;
            };
            buttons.Children.Add(newProjectButton);

            Button editMemoButton = new Button { FontWeight = FontWeight.Bold, Content = "Edytuj notatkę dla tej lekcji" };
            editMemoButton.Click += (sender, args) =>
            {
                MainWindow.Content = new LessonMemoDialog(MainWindow, this, Path.Combine(SavedDataReader.DataPath, SubjectName, LessonName, "notatka.txt")).Content;
            };
            buttons.Children.Add(editMemoButton);

            Button goBackButton = new Button() { FontWeight = FontWeight.Bold, Content = "Powróć do poprzedniego menu" };
            goBackButton.Click += (sender, args) => { LastWindow.Refresh(); MainWindow.Content = LastWindow.Content; };
            buttons.Children.Add(goBackButton);
        }
    }
}
