﻿using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Avalonia.Media;
using LessonTools.Models;
using LessonTools.OS;
using System.IO;
using System.Linq;

namespace LessonTools.Views
{
    public class ProjectManipulatorWindow : UserControl
    {
        private MainWindow MainWindow;
        private ProjectPickerWindow LastWindow;
        private string SubjectName, LessonName;
        private SchoolProject Project;

        public ProjectManipulatorWindow(MainWindow mainWindow, ProjectPickerWindow lastWindow, string subjectName, string lessonName, SchoolProject project)
        {
            MainWindow = mainWindow;
            LastWindow = lastWindow;
            SubjectName = subjectName;
            LessonName = lessonName;
            Project = project;

            this.InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);

            this.FindControl<TextBlock>("ChosenSubject").Text = SubjectName;
            this.FindControl<TextBlock>("ChosenLesson").Text = LessonName;
            this.FindControl<TextBlock>("ChosenProject").Text = Project.Name;

            StackPanel actionButtons = this.FindControl<StackPanel>("ActionButtons");

            Button openInExplorer = new Button();
            openInExplorer.Content = "Otwórz projekt w eksploratorze plików";
            openInExplorer.Click += Event_OpenInExplorer;
            actionButtons.Children.Add(openInExplorer);

            Button openInVscode = new Button();
            openInVscode.Content = "Otwórz projekt w VisualStudio Code";
            openInVscode.Click += Event_OpenInVscode;
            actionButtons.Children.Add(openInVscode);

            if (Project.Type == SchoolProjectType.HTML || Project.Type == SchoolProjectType.PHP)
            {
                Button shareOnLocalhost = new Button();
                shareOnLocalhost.Content = "Udostępnij stronę w sieci lokalnej";
                shareOnLocalhost.Click += Event_ShareOnLocalhost;
                actionButtons.Children.Add(shareOnLocalhost);

            }
            else
            {
                Button runInTerminal = new Button();
                runInTerminal.Content = "Otwórz projekt w terminalu";
                runInTerminal.Click += Event_OpenInTerminal;
                actionButtons.Children.Add(runInTerminal);
            }

            Button goBack = new Button();
            goBack.FontWeight = FontWeight.Bold;
            goBack.Content = "Powrót do poprzedniego menu";
            goBack.Click += (sender, args) => { MainWindow.Content = LastWindow.Content; };
            actionButtons.Children.Add(goBack);
        }


        private void Event_OpenInExplorer(object sender, RoutedEventArgs e)
        {
            if (UserOperatingSystem.UserOS == OS.OS.Linux)
                new ShellCommand($"xdg-open '{Project.DirectoryPath}'").Call();
            else
                new ShellCommand($"explorer.exe \"{Project.DirectoryPath}\"").Call();
        }

        private void Event_OpenInVscode(object sender, RoutedEventArgs e)
        {
            new ShellCommand($"code '{Project.DirectoryPath}'").Call();
        }

        private void Event_OpenInTerminal(object sender, RoutedEventArgs e)
        {
            new ShellCommand($"xfce4-terminal --working-directory='{Project.DirectoryPath}' --hold", false).Call();
        }

        private void Event_ShareOnLocalhost(object sender, RoutedEventArgs e)
        {
            new ShellCommand("rm -R /srv/http/*").Call();
            new ShellCommand("ln -s /usr/share/webapps/phpMyAdmin /srv/http/").Call();

            var dirs = Directory.GetDirectories(Project.DirectoryPath, "*", SearchOption.TopDirectoryOnly);
            var files = Directory.GetFiles(Project.DirectoryPath, "*", SearchOption.TopDirectoryOnly);

            foreach (string path in dirs.Concat(files))
            {
                string srvPath = Path.Combine("/srv/http", Path.GetFileName(path));
                new ShellCommand($"ln -s '{path}' '{srvPath}'").Call();
            }

            this.FindControl<TextBlock>("LocalhostShared").IsVisible = true;
        }
    }
}
